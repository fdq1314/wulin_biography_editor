﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaskEditor
{
    abstract class ZTask
    {
        public abstract void load(ZReader reader);
        public abstract void save(ZWriter writer);
    }
}
