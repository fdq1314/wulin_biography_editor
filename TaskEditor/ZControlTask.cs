﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TaskEditor
{
    public partial class ZControlTask : UserControl
    {
        private ZTaskFile zTaskFile = new ZTaskFile();

        private List<ZTask> zTempFilter;

        private List<TreeNode> zTempFindNodes = new List<TreeNode>();
        private int pos = 0;
        public ZControlTask()
        {
            InitializeComponent();
        }

        public void zLoadfile(string filename = null)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                if (filename == null)
                {
                    DialogResult dr = openFileDialog1.ShowDialog();
                    if (dr == DialogResult.OK)
                    {
                        filename = openFileDialog1.FileName;
                    }
                    else
                    {
                        return;
                    }
                }

                zTaskFile = new ZTaskFile();
                zTaskFile.load(filename);
                label1.Text = $"版本：{zTaskFile.zHeader.Version}，任务数：{zTaskFile.zHeader.TaskNum}";
                zTempFilter = zTaskFile.zTasks;
                refreshTaskList();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

            Cursor = Cursors.Default;

        }
        public void zSavefile()
        {
            DialogResult dr = saveFileDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                zTaskFile.save(saveFileDialog1.FileName);
                MessageBox.Show("保存成功");
            }
        }
        private void refreshTaskList()
        {
            listBox1.Items.Clear();
            listBox1.BeginUpdate();
            int i = 0;
            foreach (var item in zTempFilter)
            {
                listBox1.Items.Add(i + "-" + item.ToString());
                i++;
            }
            listBox1.EndUpdate();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            int n = listBox1.SelectedIndex;
            if (n >= 0)
            {
                ZTask t = zTempFilter[n];
                
                n = zTaskFile.zTasks.IndexOf(t);

                //buildView(t, root);
                ZMyNode root = new ZMyNode(t, t.ToString());
                root.zRefresh();
                treeView1.BeginUpdate();
                treeView1.Nodes.Clear();
                treeView1.Nodes.Add(root);
                treeView1.EndUpdate();
                root.Expand();
                //treeView1.ExpandAll();
            }


        }

        private void panel3_DragDrop(object sender, DragEventArgs e)
        {
            string[] s = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            if (s.Length > 0)
                zLoadfile(s[0]);
        }

        private void panel3_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.All;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void button_filter_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBox_filter.Text))
            {
                zTempFilter = zTaskFile.zTasks;
            }
            else
            {
                zTempFilter = zTaskFile.zTasks.FindAll(p => p.ToString().ToUpper().Contains(textBox_filter.Text.ToUpper()));
            }
            refreshTaskList();
        }

        private void findText(string text, TreeNode top)
        {
            if (top.Text.ToUpper().Contains(text))
            {
                zTempFindNodes.Add(top);
            }
            foreach (TreeNode item in top.Nodes)
            {
                findText(text, item);
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            zTempFindNodes.Clear();

            findText(textBox1.Text.ToUpper(), treeView1.Nodes[0]);

            pos = 0;
            if (zTempFindNodes.Count > pos)
            {
                treeView1.Focus();

                treeView1.SelectedNode = zTempFindNodes[pos];
                zTempFindNodes[pos].Expand();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            pos++;
            if (pos > zTempFindNodes.Count)
            {
                pos = zTempFindNodes.Count - 1;
            }
            if (pos < 0)
            {
                pos = 0;
            }
            if (zTempFindNodes.Count > pos)
            {
                treeView1.Focus();
                treeView1.SelectedNode = zTempFindNodes[pos];
                zTempFindNodes[pos].Expand();

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            pos--;
            if (pos < 0)
            {
                pos = 0;
            }
            if (zTempFindNodes.Count > pos)
            {
                treeView1.Focus();

                treeView1.SelectedNode = zTempFindNodes[pos];
                zTempFindNodes[pos].EnsureVisible();
                zTempFindNodes[pos].Expand();


            }
        }

        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            ZMyNode node = e.Node as ZMyNode;
            if (node.Nodes.Count > 0)
            {
                return;
            }
            treeView1.BeginUpdate();
            try
            {
                ZEditorForm editorForm = new ZEditorForm(node.Text, node.zStringValue);
                DialogResult dr = editorForm.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    node.zSetValue(editorForm.zValue);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            treeView1.EndUpdate();

        }
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            ZTASK401 zTASK401 = new ZTASK401();
            zTaskFile.zTasks.Add(zTASK401);
            zTempFilter = zTaskFile.zTasks;
            refreshTaskList();
        }

        private void buttonDel_Click(object sender, EventArgs e)
        {
            int n = listBox1.SelectedIndex;
            if (n >= 0)
            {
                ZTask t = zTempFilter[n];
                zTempFilter.Remove(t);
                zTaskFile.zTasks.Remove(t);
                refreshTaskList();
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            ZMyNode node = e.Node as ZMyNode;
            node.zRefresh();
        }
    }
}
