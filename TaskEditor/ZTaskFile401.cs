﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace TaskEditor
{

    class ZPTR<T> where T:class
    {
        public int stack_nouse = 0x7FFFFFFF;
        public T _data;
        public ZPTR(T value=null)
        {
            if(value==null)
            {
                value =Activator.CreateInstance<T>();
            }
            _data = value;

        }
    }
    class ZPTRList<T>
    {
        public int nouse = 0x7FFFFFFF;
        public List<T> _data = new List<T>();
    }
    class ZVECTOR<T>
    {
        public int a = 0, b = 0, c = 0, size = 0;
        public List<T> _data = new List<T>();
    }
    class ZTASK_TIME
    {
        public int year = 0, month = 0, day = 0, hour = 0, min = 0, weekday = 0;
    }
    class ZZONE_VERT
    {
        public float x = 0, y = 0, z = 0;
    }
    class ZITEM_WANTED
    {
        public uint zItemTemplId = 0;
        public bool zCommonItem = true;
        public uint zItemNum = 0;
        public float zProb = 0;
        public bool zBind = false;
        public int zPeriod = 0;
        public bool zTimetable = false;
        public byte zDayOfWeek = 0;
        public byte zHour = 0;
        public byte zMinute = 0;
        public int zEStoneNum = 0;
    }
    class ZMONSTER_WANTED
    {
        public uint zMonsterTemplId = 0;
        public uint zMonsterNum = 0;
        public uint zDropItemId = 0;
        public uint zDropItemCount = 0;
        public int zDropItemEStone = 0;
        public bool zDropCmnItem = false;
        public float zDropProb = 0;
        public bool zKillerLev = false;
    }
    class ZTEAM_MEM_WANTED
    {
        public uint zLevelMin = 0;
        public uint zLevelMax = 0;
        public uint zRace = 0;
        public uint zOccupation = 0;
        public uint zGender = 0;
        public uint zMinCount = 0;
        public uint zMaxCount = 0;
        public uint zTask = 0;
    }
    class ZFINISH_TASK_COUNT_INFO
    {
        public uint task_id = 0;
        public ushort count = 0;
    }
    class ZTASK_EXPRESSION
    {
        public int type = 0;
        public float value = 0;
    }
    class ZCOMPARE_EXPRESSION
    {
        public bool bNeedComp = false;
        public ZVECTOR<byte> strExpLeft = new ZVECTOR<byte>();
        public ZTASK_EXPRESSION[] arrExpLeft = new ZTASK_EXPRESSION[2];
        public int nCompOper = 0;
        public ZVECTOR<byte> strExpRight = new ZVECTOR<byte>();
        public ZTASK_EXPRESSION[] arrExpRight = new ZTASK_EXPRESSION[2];
    }
    class ZPROTEC_NPC_INFO
    {
        public int npc_id = 0;
        public int count = 0;
        public float x = 0;
        public float y = 0;
        public float z = 0;
        public int life_span = 0;
        public bool is_guard = false;
        public int path_id = 0;
    }

    class ZAward_Items_Cand : ZSeriable
    {
        public bool zRandChoose = false;
        public uint zAwardItems = 0;
        public List<ZITEM_WANTED> _AwardItems = new List<ZITEM_WANTED>();
        public override void read(ZReader reader)
        {
            if (zAwardItems > 0 && zAwardItems < 300)
            {
                for (int i = 0; i < zAwardItems; i++)
                {
                    ZITEM_WANTED iw = reader.readt<ZITEM_WANTED>();
                    _AwardItems.Add(iw);
                }
            }
            else if (zAwardItems != 0)
            {
                throw new Exception("ZAward_Items_Cand中数目异常：" + zAwardItems);
            }

        }
        public override void beforeWrite()
        {
            zAwardItems = (uint)_AwardItems.Count;
        }
        public override void write(ZWriter writer)
        {
            foreach (var item in _AwardItems)
            {
                writer.writeObject(item);
            }

        }
    }
    class ZAWard_Data : ZSeriable
    {
        public uint zGoldNum = 0;
        public uint zExp = 0;
        public uint zNewTask = 0;
        public uint zSP = 0;
        public uint zReputation = 0;
        public int zContribution = 0;
        public uint zProsperity = 0;
        public int zTitle = 0;
        public int zPKValue = 0;//36
        public bool zResetPKValue = false;
        public bool zDivorce = false;//38
        public int[] zFriendships = new int[16];//64+38=102
        public uint zNewPeriod = 0;
        public uint zNewRelayStation = 0;
        public uint zStorehouseSize = 0;
        public uint zFactionStorehouseSize = 0;
        public int zInventorySize = 0;
        public uint zPetInventorySize = 0;
        public int zFashionInvSize = 0;
        public int zRideInvSize = 0;
        public uint zFuryULimit = 0;//102+36=138
        public bool zSetProduceSkill = false;//139
        public uint zProduceSkillExp = 0;
        public uint zNewProfession = 0;
        public uint zTransWldId = 0;//12+139=151
        public ZZONE_VERT zTransPt = new ZZONE_VERT();//151+12=163
        public int zMonsCtrl = 0;//163+4=167
        public bool zTrigCtrl = false;//168
        public int zBuffId = 0;
        public int zBuffLev = 0;
        public bool zRegenerate = false;
        public bool zSendMsg = false;//178
        public int zMsgChannel = 0;
        public bool zForgetSkill = false;
        public bool zForgetSkillAll = false;
        public uint zClearCountTask = 0;
        public uint zCandItemCount = 0;//14+178=192
        public ZPTRList<ZAward_Items_Cand> zCandItems = new ZPTRList<ZAward_Items_Cand>();//196
        public int zMultiGlobalKey = 0;//200
        public ZVECTOR<int> zChangeKeyArr = new ZVECTOR<int>();//16
        public ZVECTOR<int> zChangeKeyValueArr = new ZVECTOR<int>();//16
        public ZVECTOR<byte> zChangeTypeArr = new ZVECTOR<byte>();//16
        public override void read(ZReader reader)
        {
            if (zCandItemCount > 300)
            {
                throw new Exception("ZAWARD_DATA中CandItems中数目异常：" + zCandItemCount);
            }
            if (zCandItemCount > 0)
            {
                for (int i = 0; i < zCandItemCount; i++)
                {
                    zCandItems._data.Add(reader.readt<ZAward_Items_Cand>());
                }
            }
            if (zChangeKeyArr.size > 0)
            {
                int n = zChangeKeyArr.size;
                for (int i = 0; i < n; i++)
                {
                    zChangeKeyArr._data.Add(reader.readi());
                    zChangeKeyValueArr._data.Add(reader.readi());
                    zChangeTypeArr._data.Add(reader.readb());
                }

            }
        }
        public override void beforeWrite()
        {
            zCandItemCount = (uint)zCandItems._data.Count;
            zChangeKeyArr.size = zChangeKeyArr._data.Count;
        }
        public override void write(ZWriter writer)
        {

            foreach (var item in zCandItems._data)
            {
                writer.writeObject(item);
            }
            int n = zChangeKeyArr.size;
            for (int i = 0; i < n; i++)
            {
                writer.writeObject(zChangeKeyArr._data[i]);
                writer.writeObject(zChangeKeyValueArr._data[i]);
                writer.writeObject(zChangeTypeArr._data[i]);
            }
        }
        //248
    }
    class ZAward_Ratio_Scale : ZSeriable
    {
        public int zScales = 0;
        public float[] zRatios = new float[5];
        public List<ZAWard_Data> _Awards = new List<ZAWard_Data>();
        public override void read(ZReader reader)
        {
            if (zScales > 0 && zScales < 300)
            {
                for (int i = 0; i < zScales; i++)
                {
                    _Awards.Add(reader.readt<ZAWard_Data>());
                }
            }
            else if (zScales != 0)
            {
                throw new Exception("ZAward_Ratio_Scale中数目异常：" + zScales);
            }
        }
        public override void beforeWrite()
        {
            zScales = _Awards.Count;
        }
        public override void write(ZWriter writer)
        {
            foreach (var item in _Awards)
            {
                writer.writeObject(item);
            }
        }
    }
    class ZAward_Items_Scale : ZSeriable
    {
        public int zScales = 0;
        public int zItemId = 0;
        public int[] zCounts = new int[5];
        public List<ZAWard_Data> _Awards = new List<ZAWard_Data>();
        public override void read(ZReader reader)
        {
            if (zScales > 0 && zScales < 300)
            {
                for (int i = 0; i < zScales; i++)
                {
                    _Awards.Add(reader.readt<ZAWard_Data>());
                }
            }
            else if (zScales != 0)
            {
                throw new Exception("ZAward_Items_Scale中数目异常：" + zScales);
            }
        }

        public override void beforeWrite()
        {
            zScales = _Awards.Count;
        }
        public override void write(ZWriter writer)
        {
            foreach (var item in _Awards)
            {
                writer.writeObject(item);
            }
        }
    }
    class ZTaskFixedData : ZSeriable
    {
        #region segs
        public uint zID = 0;
        public string zName_60 = "新建主线任务";
        public bool zHasSign = false;
        public ZPTR<string> zSignature_60 = new ZPTR<string>("");
        public uint zType = 0;
        public uint zTimeLimit = 0;
        public bool zAbsTime = false;
        public uint zTimetable = 0;
        public byte[] ztmType = new byte[12];
        public ZPTRList<ZTASK_TIME> zTmStart = new ZPTRList<ZTASK_TIME>();
        public ZPTRList<ZTASK_TIME> zTmEnd = new ZPTRList<ZTASK_TIME>();
        public int zAvailFrequency = 0;
        public int zTimeInterval = 0;
        public bool zChooseOne = false;
        public bool zRandOne = false;
        public bool zExeChildInOrder = false;
        public bool zParentAlsoFail = false;
        public bool zParentAlsoSucc = false;
        public bool zCanGiveUp = false;
        public bool zCanRedo = false;
        public bool zCanRedoAfterFailure = false;
        public bool zClearAsGiveUp = false;
        public bool zNeedRecord = false;
        public bool zFailAsPlayerDie = false;
        public bool zFailAsOffLine = false;
        public uint zMaxReceiver = 0;
        public bool zDelvInZone = false;
        public uint zDelvWorld = 0;
        public ZZONE_VERT zDelvMinVert = new ZZONE_VERT();
        public ZZONE_VERT zDelvMaxVert = new ZZONE_VERT();
        public bool zTransTo = false;
        public uint zTransWldId = 0;
        public ZZONE_VERT zTransPt = new ZZONE_VERT();
        public int zMonsCtrl = 0;
        public bool zTrigCtrl = false;
        public bool zAutoDeliver = false;
        public bool zDeathTrig = false;
        public bool zClearAcquired = false;
        public uint zSuitableLevel = 0;
        public bool zShowPrompt = false;
        public bool zKeyTask = false;
        public uint zDelvNPC = 0;
        public uint zAwardNPC = 0;
        public bool zSkillTask = false;
        public bool zCanSeekOut = false;
        public bool zShowDirection = false;
        public float zStorageWeight = 0;
        public uint zRank = 0;
        public bool zMarriage = false;
        public bool zNoClearByRegenerate = false;
        public bool zRecFinishCount = false;
        public bool zRecFinishCountGlobal = false;
        public uint zMaxFinishCount = 0;
        public ZTASK_TIME zFinishClearTime = new ZTASK_TIME();
        public int zFinishTimeType = 0;
        public bool zHidden = false;
        public uint zPremise_Lev_Min = 0;
        public uint zPremise_Lev_Max = 0;
        public bool zShowByLev = false;
        public int zLevel2Mask = 0;
        public uint zPremItems = 0;
        public ZPTRList<ZITEM_WANTED> 前提物品要求 = new ZPTRList<ZITEM_WANTED>();
        public bool 保留前提物品 = false;
        public bool zShowByItems = false;
        public uint zGivenItems = 0;
        public uint zGivenCmnCount = 0;
        public uint zGivenTskCount = 0;
        public ZPTRList<ZITEM_WANTED> pGivenItems = new ZPTRList<ZITEM_WANTED>();
        public ZPTRList<short> pPremTitles = new ZPTRList<short>();
        public uint zPremTitleCount = 0;
        public bool zTitleOneOk = false;
        public short zTitleOnHead = 0;
        public uint zPremise_Deposit = 0;
        public bool zShowByDeposit = false;
        public int zPremise_Reputation = 0;
        public bool zRepuDeposit = false;
        public bool zShowByRepu = false;
        public int zPremise_Contribution = 0;
        public bool zDepositContribution = false;
        public bool zZoneRepuDeposit = false;
        public int[] zPremise_Friendship = new int[16];
        public uint zPremise_Task_Count = 0;
        public uint[] zPremise_Tasks = new uint[5];
        public bool zShowByPreTask = false;
        public bool zPreTaskOneOk = false;
        public uint zPremFinishTaskCount = 0;
        public ZFINISH_TASK_COUNT_INFO[] zPremFinishTasks = new ZFINISH_TASK_COUNT_INFO[5];
        public uint zPremGlobalCount = 0;
        public uint zPremGlobalTask = 0;
        public uint zPremise_Period = 0;
        public bool zShowByPeriod = false;
        public uint zPremise_Faction = 0;
        public bool zShowByFaction = false;
        public bool zPremise_Master = false;
        public uint zGender = 0;
        public bool zShowByGender = false;
        public uint zOccupationCounts = 0;
        public uint[] zOccupations = new uint[8];
        public bool zShowByOccup = false;
        public bool zPremise_Spouse = false;
        public bool zShowBySpouse = false;
        public uint zPremise_Cotask = 0;
        public uint zCoTaskCond = 0;
        public uint zMutexTaskCount = 0;
        public uint[] zMutexTasks = new uint[5];
        public int[] zSkillLev = new int[4];
        public byte zDynTaskType = 0;
        public uint zSpecialAward = 0;
        public int zPKValueMin = 0;
        public int zPKValueMax = 0;
        public bool zPremise_GM = false;
        public bool 组队任务 = false;
        public bool zRcvByTeam = false;
        public bool zSharedTask = false;
        public bool zSharedAchieved = false;
        public bool zCheckTeammate = false;
        public float zTeammateDist = 0;
        public bool zAllFail = false;
        public bool zCapFail = false;
        public bool zCapSucc = false;
        public float zSuccDist = 0;
        public bool zDismAsSelfFail = false;
        public bool zRcvChckMem = false;
        public float zRcvMemDist = 0;
        public bool zCntByMemPos = false;
        public float zCntMemDist = 0;
        public uint zTeamMemsWanted = 0;
        public ZPTRList<ZTEAM_MEM_WANTED> 队员要求 = new ZPTRList<ZTEAM_MEM_WANTED>();
        public bool zShowByTeam = false;
        public int zModelChange = 0;
        public ZCOMPARE_EXPRESSION zPremCompExp = new ZCOMPARE_EXPRESSION();
        public int zRankScore = 0;
        public bool zRankScoreDeposit = false;
        public uint zenumMethod = 0;
        public uint zenumFinishType = 0;
        public uint zMonsterWanted = 0;
        public ZPTRList<ZMONSTER_WANTED> 杀怪要求 = new ZPTRList<ZMONSTER_WANTED>();
        public uint zItemsWanted = 0;
        public ZPTRList<ZITEM_WANTED> 物品要求 = new ZPTRList<ZITEM_WANTED>();
        public uint 金钱需求 = 0;
        public ZPTRList<ZPROTEC_NPC_INFO> pNPCProtect = new ZPTRList<ZPROTEC_NPC_INFO>();
        public int zNPCProtectCount = 0;
        public int zNPCProtectTotalTime = 0;
        public bool zNPCProtectDieCond = false;
        public bool zNPCProtectDieSuccess = false;
        public int zNPCProtectDieCount = 0;
        public bool zNPCProtectReachSiteCond = false;
        public bool zNPCProtectReachSiteSuccess = false;
        public int zNPCProtectReachSiteCount = 0;
        public ZZONE_VERT zReachSiteMin = new ZZONE_VERT();
        public ZZONE_VERT zReachSiteMax = new ZZONE_VERT();
        public uint zReachSiteId = 0;
        public uint 等待时间 = 0;
        public uint zTitleWantedNum = 0;
        public short[] zTitleWanted = new short[5];
        public uint zFinishLev = 0;
        public ZCOMPARE_EXPRESSION zFinCompExp = new ZCOMPARE_EXPRESSION();
        public uint zAwardType_S = 0;
        public uint zAwardType_F = 0;
        public ZPTR<ZAWard_Data> pAward_S = new ZPTR<ZAWard_Data>();
        public ZPTR<ZAWard_Data> pAward_F = new ZPTR<ZAWard_Data>();
        public ZPTR<ZAward_Ratio_Scale> pAwByRatio_S = new ZPTR<ZAward_Ratio_Scale>();
        public ZPTR<ZAward_Ratio_Scale> pAwByRatio_F = new ZPTR<ZAward_Ratio_Scale>();
        public ZPTR<ZAward_Items_Scale> pAwByItems_S = new ZPTR<ZAward_Items_Scale>();
        public ZPTR<ZAward_Items_Scale> pAwByItems_F = new ZPTR<ZAward_Items_Scale>();
        public uint zParent = 0;
        public uint zPrevSibling = 0;
        public uint zNextSibling = 0;
        public uint zFirstChild = 0;
        #endregion
        public override void read(ZReader reader)
        {
            if (zHasSign)
            {
                byte[] buff = new byte[60];
                byte[] data = reader.readbs(60);
                zSignature_60._data = Encoding.Unicode.GetString(data);
            }
            if (zTimetable > 0)
            {
                for (int i = 0; i < zTimetable; i++)
                {
                    zTmStart._data.Add(reader.readt<ZTASK_TIME>());
                    zTmEnd._data.Add(reader.readt<ZTASK_TIME>());
                }
            }
            if (zPremItems > 0)
            {
                for (int i = 0; i < zPremItems; i++)
                {
                    前提物品要求._data.Add(reader.readt<ZITEM_WANTED>());
                }
            }
            if (zPremTitleCount > 0)
            {
                for (int i = 0; i < zPremTitleCount; i++)
                {
                    pPremTitles._data.Add(reader.reads());
                }
            }
            if (zGivenItems > 0)
            {
                zGivenCmnCount = 0;
                zGivenTskCount = 0;
                for (int i = 0; i < zGivenItems; i++)
                {
                    ZITEM_WANTED data = reader.readt<ZITEM_WANTED>();
                    pGivenItems._data.Add(data);
                    if (data.zCommonItem)
                    {
                        zGivenCmnCount++;
                    }
                    else
                    {
                        zGivenTskCount++;
                    }
                }
            }
            if (zTeamMemsWanted > 0)
            {
                for (int i = 0; i < zTeamMemsWanted; i++)
                {
                    队员要求._data.Add(reader.readt<ZTEAM_MEM_WANTED>());
                }
            }
            if (zPremCompExp.bNeedComp)
            {
                zPremCompExp.strExpRight._data.AddRange(reader.readbs(zPremCompExp.strExpRight.size));//// no deal
                zPremCompExp.strExpLeft._data.AddRange(reader.readbs(zPremCompExp.strExpLeft.size));//// no deal
            }
            if (zMonsterWanted > 0)
            {
                for (int i = 0; i < zMonsterWanted; i++)
                {
                    杀怪要求._data.Add(reader.readt<ZMONSTER_WANTED>());
                }
            }
            if (zItemsWanted > 0)
            {
                for (int i = 0; i < zItemsWanted; i++)
                {
                    物品要求._data.Add(reader.readt<ZITEM_WANTED>());
                }
            }
            if (zNPCProtectCount > 0)
            {
                for (int i = 0; i < zNPCProtectCount; i++)
                {
                    pNPCProtect._data.Add(reader.readt<ZPROTEC_NPC_INFO>());
                }
            }
            if (zFinCompExp.bNeedComp)
            {
                zFinCompExp.strExpRight._data.AddRange(reader.readbs(zFinCompExp.strExpRight.size));//// no deal
                zFinCompExp.strExpLeft._data.AddRange(reader.readbs(zFinCompExp.strExpLeft.size));//// no deal
            }
            pAward_S._data = reader.readt<ZAWard_Data>();
            pAward_F._data = reader.readt<ZAWard_Data>();
            pAwByRatio_S._data = reader.readt<ZAward_Ratio_Scale>();
            pAwByRatio_F._data = reader.readt<ZAward_Ratio_Scale>();
            pAwByItems_S._data = reader.readt<ZAward_Items_Scale>();
            pAwByItems_F._data = reader.readt<ZAward_Items_Scale>();

        }
        public override void beforeWrite()
        {
            zHasSign = true;
            if (string.IsNullOrWhiteSpace(zSignature_60._data))
            {
                zHasSign = false;
            }
            zTimetable = (uint)zTmStart._data.Count;
            zPremItems = (uint)前提物品要求._data.Count;
            zPremTitleCount = (uint)pPremTitles._data.Count;
            zGivenItems = (uint)pGivenItems._data.Count;
            if (组队任务==false)
            {
                队员要求._data.Clear();
            }
            zTeamMemsWanted = (uint)队员要求._data.Count;
            zMonsterWanted = (uint)杀怪要求._data.Count;
            zItemsWanted = (uint)物品要求._data.Count;
        }
        public override void write(ZWriter writer)
        {
            if (zHasSign)
            {
                byte[] buff = new byte[60];
                //byte[] data = Encoding.Unicode.GetBytes(zSignature_60._data);
                //for (int i = 0; i < 60; i++)
                //{
                //    if (data.Length>i)
                //    {
                //        buff[i] = data[i];
                //    }
                //    else
                //    {
                //        break;
                //    }
                //}
                writer.writeObject(buff);
            }
            for (int i = 0; i < zTimetable; i++)
            {
                writer.writeObject(zTmStart._data[i]);
                writer.writeObject(zTmEnd._data[i]);
            }
            foreach (var item in 前提物品要求._data)
            {
                writer.writeObject(item);
            }
            foreach (var item in pPremTitles._data)
            {
                writer.writeObject(item);
            }
            foreach (var item in pGivenItems._data)
            {
                writer.writeObject(item);
            }
            foreach (var item in 队员要求._data)
            {
                writer.writeObject(item);
            }
            if (zPremCompExp.bNeedComp)
            {
                for (int i = 0; i < zPremCompExp.strExpRight.size; i++)
                {
                    writer.writeObject(zPremCompExp.strExpRight._data[i]);//// no deal
                }
                for (int i = 0; i < zPremCompExp.strExpRight.size; i++)
                {
                    writer.writeObject(zPremCompExp.strExpLeft._data[i]);//// no deal
                }

            }
            foreach (var item in 杀怪要求._data)
            {
                writer.writeObject(item);
            }
            foreach (var item in 物品要求._data)
            {
                writer.writeObject(item);
            }
            foreach (var item in pNPCProtect._data)
            {
                writer.writeObject(item);
            }
            if (zFinCompExp.bNeedComp)
            {
                for (int i = 0; i < zFinCompExp.strExpRight.size; i++)
                {
                    writer.writeObject(zFinCompExp.strExpRight._data[i]);//// no deal
                }
                for (int i = 0; i < zFinCompExp.strExpRight.size; i++)
                {
                    writer.writeObject(zFinCompExp.strExpLeft._data[i]);//// no deal
                }
            }
            writer.writeObject(pAward_S._data);
            writer.writeObject(pAward_F._data);
            writer.writeObject(pAwByRatio_S._data);
            writer.writeObject(pAwByRatio_F._data);
            writer.writeObject(pAwByItems_S._data);
            writer.writeObject(pAwByItems_F._data);

        }
    }
    class ZTalk_proc_option
    {
        public ushort id = 0;
        public ushort type = 0;
        public string text_128 = "";
        public uint param = 0;
    }
    class ZTalk_proc_window : ZSeriable
    {
        public uint id = 0;
        public int id_parent = -1;
        public int _talkTextLen = 0;
        public string _talkText = "";
        public int _nuzoption = 0;
        public List<ZTalk_proc_option> _options = new List<ZTalk_proc_option>();
        public override void write(ZWriter writer)
        {
            writer.writeObject(_talkText);
            writer.writeObject(_options.Count);
            foreach (var item in _options)
            {
                writer.writeObject(item);
            }
        }
        public override void read(ZReader reader)
        {
            _talkTextLen = reader.readi();
            _talkText = reader.readunicode(_talkTextLen * 2);
            _nuzoption = reader.readi();
            if (_nuzoption > 0)
            {
                for (int i = 0; i < _nuzoption; i++)
                {
                    _options.Add(reader.readt<ZTalk_proc_option>());
                }
            }
        }
    }
    class ZTalk_Proc : ZSeriable
    {
        public uint id_talk = 0;
        public string text_128 = "";
        public int _nuzwindow = 0;
        public List<ZTalk_proc_window> _windows = new List<ZTalk_proc_window>();

        public override void read(ZReader reader)
        {
            _nuzwindow = reader.readi();
            if (_nuzwindow > 0)
            {
                for (int i = 0; i < _nuzwindow; i++)
                {
                    _windows.Add(reader.readt<ZTalk_proc_window>());
                }
            }
        }
        public override void write(ZWriter writer)
        {
            writer.writeObject(_windows.Count);
            foreach (var item in _windows)
            {
                writer.writeObject(item);
            }
        }
    }

    class ZTASK401 : ZTask
    {
        public ZTaskFixedData zFixedData = new ZTaskFixedData();
        public string zDescript = "";
        public string zOkText = "";
        public string zNoText = "";
        public string zTribute = "";
        public ZTalk_Proc zDelvTaskTalk = new ZTalk_Proc();
        public ZTalk_Proc zUnqualifiedTalk = new ZTalk_Proc();
        public ZTalk_Proc zDelvItemTalk = new ZTalk_Proc();
        public ZTalk_Proc zExeTalk = new ZTalk_Proc();
        public ZTalk_Proc zAwardTalk = new ZTalk_Proc();
        public int zSubTaskCount = 0;
        public List<ZTASK401> zSubTasks = new List<ZTASK401>();
        public override void load(ZReader reader)
        {
            reader.readObject(zFixedData);

            int n = reader.readi();
            zDescript = reader.readunicode(2 * n);
            n = reader.readi();
            zOkText = reader.readunicode(2 * n);
            n = reader.readi();
            zNoText = reader.readunicode(2 * n);
            n = reader.readi();
            zTribute = reader.readunicode(2 * n);

            reader.readObject(zDelvTaskTalk);
            reader.readObject(zUnqualifiedTalk);
            reader.readObject(zDelvItemTalk);
            reader.readObject(zExeTalk);
            reader.readObject(zAwardTalk);

            zSubTaskCount = reader.readi();

            for (int i = 0; i < zSubTaskCount; i++)
            {
                ZTASK401 task = new ZTASK401();
                task.load(reader);
                zSubTasks.Add(task);
            }
        }
        public override void save(ZWriter writer)
        {
            writer.writeObject(zFixedData);

            writer.writeObject(zDescript);
            writer.writeObject(zOkText);
            writer.writeObject(zNoText);
            writer.writeObject(zTribute);

            writer.writeObject(zDelvTaskTalk);
            writer.writeObject(zUnqualifiedTalk);
            writer.writeObject(zDelvItemTalk);
            writer.writeObject(zExeTalk);
            writer.writeObject(zAwardTalk);

            writer.writeObject(zSubTasks.Count);
            foreach (var item in zSubTasks)
            {
                item.save(writer);
            }
        }
        public override string ToString()
        {
            return $"{zFixedData.zID}:{zFixedData.zName_60}";
        }
    }

}
